package homework8;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

public class Main {
    public static void main(String[] args) {
        Woman mother = new Woman("Mary", "Mead", 1970);
        Man father = new Man("Tom", "Red", 1967);
        Dog dog = new Dog("Rock", 5, 75, new HashSet<>(Arrays.asList("eat", "drink", "sleep")));
        Man child_first = new Man("Michael", "Smith", 1990, 110, new HashMap<String, String>() {{
            put(DayOfWeek.SUN.getFullNameOfTheDay(), "go to fishing");
            put(DayOfWeek.MON.getFullNameOfTheDay(), "go to gym");
        }});
        Woman child_second = new Woman("Ariel", "Jones", 1995, 90, new HashMap<String, String>() {{
            put(DayOfWeek.SUN.getFullNameOfTheDay(), "relax");
        }});
        Man child_third = new Man("Bob", "Williams", 1998, 110, new HashMap<String, String>() {{
            put(DayOfWeek.THUR.getFullNameOfTheDay(), "do the shopping");
        }});

        Family family = new Family(mother, father);
        family.setPet(dog);

        family.addChild(child_first);
        family.addChild(child_second);
        family.addChild(child_third);

        System.out.println("----------------------");
        System.out.println(family);
        System.out.println("countFamily: " + family.countFamily());
        System.out.printf("deleteChild: %s \n", family.deleteChild(child_third));
        System.out.println("countFamily: " + family.countFamily());

        System.out.println("----------------------");
        child_first.greetPet();
        child_first.describePet();
        System.out.println(child_first.feedPet(true));
        System.out.println(child_first.feedPet(false));
        System.out.println(child_first.feedPet(false));
        System.out.println(child_first.feedPet(false));

        System.out.println("----------------------");
        dog.eat();
        dog.respond();
        dog.foul();
    }
}
