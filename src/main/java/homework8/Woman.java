package homework8;

import java.util.HashMap;

public final class Woman extends Human {
    public Woman(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Woman(String name, String surname, int year, int iq, HashMap<String, String> schedule) {
        super(name, surname, year, iq, schedule);
    }

    public void makeup(){
        System.out.println("Makeup");
    }

    @Override
    public void greetPet(){
        if (family.getPets().size() > 0){
            for (Pet pet : family.getPets()) {
                System.out.printf("Привет, %s! \n", pet.nickname);
            }
        } else {
            System.out.println("У меня нет домашнего животного!");
        }
    }

    @Override
    public String toString() {
        return "Woman{" + "\n" +
                "\t name='" + name + '\'' + ",\n" +
                "\t surname='" + surname + '\'' + ",\n" +
                "\t year=" + year + ",\n" +
                "\t iq=" + iq + ",\n" +
                "\t schedule=" + schedule.toString() + ",\n" +
                '}';
    }
}
