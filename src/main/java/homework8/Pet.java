package homework8;

import java.util.HashSet;
import java.util.Set;

public abstract class Pet {
     String nickname;
     int age;
     int trickLevel;
     Set<String> habits;

    public Pet(String nickname, int age, int trickLevel, HashSet<String> habits) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public void eat(){
        System.out.println("Я кушаю!");
    }

    public abstract void respond();
}
