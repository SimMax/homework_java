package homework8;

import java.util.HashMap;

public final class Man extends Human {
    public Man(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Man(String name, String surname, int year, int iq, HashMap<String, String> schedule) {
        super(name, surname, year, iq, schedule);
    }

    public void repairCar(){
        System.out.println("RepairCar");
    }


    @Override
    public void greetPet(){
        if (family.getPets().size() > 0){
            for (Pet pet : family.getPets()) {
                System.out.printf("Привет, %s! \n", pet.nickname);
            }
        } else {
            System.out.println("У меня нет домашнего животного!");
        }
    }

    @Override
    public String toString() {
        return "Man{" + "\n" +
                "\t name='" + name + '\'' + ",\n" +
                "\t surname='" + surname + '\'' + ",\n" +
                "\t year=" + year + ",\n" +
                "\t iq=" + iq + ",\n" +
                "\t schedule=" + schedule.toString() + ",\n" +
                '}';
    }
}
