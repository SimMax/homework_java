package homework8;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Objects;

public class Family {
    private Human mother;
    private Human father;
    private ArrayList<Human> children;
    private HashSet<Pet> pets;

    static {
        System.out.println("Загружается класс Family");
    }

    {
        System.out.println("Family: новый экземпляр");
    }

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        this.children = new ArrayList<>();
        this.pets = new HashSet<>();
        this.mother.setFamily(this);
        this.father.setFamily(this);
    }

    public void setPet(Pet pet) {
        if(!pets.contains(pet)){
            pets.add(pet);
        } else {
            System.out.println("Животное уже было добавлено!");
        }
    }

    public HashSet<Pet> getPets() {
        return pets;
    }

    public void addChild(Human child) {
        if(!children.contains(child)){
            child.setFamily(this);
            children.add(child);
        } else {
            System.out.println("Ребёнок уже есть в семье!");
        }
    }

    public boolean deleteChild(Human child) {
        boolean flag = false;
        for(Human item : children){
            if (child.equals(item)){
                item.setFamily(null);
                children.remove(child);
                flag = true;
                break;
            }
        }
        return flag;
    }

    public boolean deleteChild(int position) {
        if(position < 0 || position >= children.size()){
            return false;
        } else {
            children.get(position).setFamily(null);
            children.remove(position);
            return true;
        }
    }

    public int countFamily() {
        return children.size() + (mother == null ? 0 : 1) + (father == null ? 0 : 1);
    }

    public ArrayList<Human> _get_children_for_test(){
        return children;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(mother, family.mother) &&
                Objects.equals(father, family.father) &&
                Objects.equals(children, family.children) &&
                Objects.equals(pets, family.pets);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mother, father, children, pets);
    }

    @Override
    public String toString() {
        return "Family{" +
                "mother=" + mother +
                ", father=" + father +
                ", children=" + children +
                ", pets=" + pets +
                '}';
    }

    @Override
    public void finalize(){
        System.out.println("Deleted: " + this.toString());
    }
}
