package homework8;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Human {
    public final String name;
    public final String surname;
    public final int year;
    public Family family;
    public int iq;
    public Map<String, String> schedule;

    static {
        System.out.println("Загружается класс Human");
    }

    {
        System.out.println("Human: новый экземпляр");
    }

    public Human(String name, String surname, int year) {
       this(name, surname, year,  90, new HashMap<>());
    }

    public Human(String name, String surname, int year, int iq, Map<String, String> schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public Family getFamily() {
        return this.family;
    }

    public void greetPet(){
        if (family.getPets().size() > 0){
            for (Pet pet : family.getPets()) {
                System.out.printf("Привет, %s! \n", pet.nickname);
            }
        } else {
            System.out.println("У меня нет домашнего животного!");
        }
    }

    public boolean feedPet(boolean flag){
        if (family.getPets().size() > 0){
            if(flag){
                for (Pet pet : family.getPets()) {
                    System.out.printf("Я покормил %s. \n", pet.nickname);
                }
                flag = true;
            } else {
                for (Pet pet : family.getPets()) {
                    if( pet.trickLevel > (int)(Math.random()*101)){
                        System.out.printf("Хм... покормлю ка я %s. \n", pet.nickname);
                        flag = true;
                    } else {
                        System.out.printf("Думаю, %s не голоден. \n", pet.nickname);
                        flag = false;
                    }
                }
            }
        } else {
            System.out.println("У меня нет домашнего животного!");
            flag = false;
        }
        return flag;
    }

    public void describePet(){
        if (family.getPets().size() > 0){
            for (Pet pet : family.getPets()) {
                System.out.printf("У меня есть %s, ему %s лет, он %s. \n", pet.nickname, pet.age,
                        pet.trickLevel > 50 ? "очень хитрый" : "почти не хитрый");
            }
        } else {
            System.out.println("У тебя нет домашнего животного!");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return year == human.year &&
                Objects.equals(name, human.name) &&
                Objects.equals(surname, human.surname) &&
                Objects.equals(family, human.family);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, year, family, iq, schedule);
    }

    @Override
    public void finalize(){
        System.out.println("Deleted: " + this.toString());
    }
}
