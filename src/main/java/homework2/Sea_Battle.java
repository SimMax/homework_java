package homework2;

import java.util.Arrays;
import java.util.Scanner;

public class Sea_Battle {
    public static int getRandomNumber() {
        return (int)(Math.random()*5 + 1);
    }

    public static int getUsersNumber(String params) {
        Scanner scan = new Scanner(System.in);
        int number;
        do {
            System.out.printf("Enter a %s number from 1 to 5: \n", params);
            while (!scan.hasNextInt()) {
                System.out.printf("Error! Enter a %s number from 1 to 5: \n", params);
                scan.next();
            }
            number = scan.nextInt();
            if (number < 1 || number > 5){
                System.out.printf("Error! Enter a %s number from 1 to 5: \n", params);
                scan.next();
            }
        } while (number < 1 || number > 5);
        return number;
    }

    public static void paintBattlefield(String[][] battlefield){
        for(String[] line : battlefield){
            System.out.println(Arrays.toString(line));
        }
    }

    public static void main(String[] args) {
        String[][] battlefield = {
                {"0", "1", "2", "3", "4", "5"},
                {"1", "-", "-", "-", "-", "-"},
                {"2", "-", "-", "-", "-", "-"},
                {"3", "-", "-", "-", "-", "-"},
                {"4", "-", "-", "-", "-", "-"},
                {"5", "-", "-", "-", "-", "-"},
        };

        int shipPositionLine = getRandomNumber();
        int shipPositionColumn = getRandomNumber();

        System.out.println("All set. Get ready to rumble!");
        paintBattlefield(battlefield);

        while (true){
            int usersLine = getUsersNumber("line");
            int usersColumn = getUsersNumber("column");

            if(usersLine == shipPositionLine && usersColumn == shipPositionColumn){
                battlefield[usersLine][usersColumn] = "x";
                paintBattlefield(battlefield);
                System.out.println("You have won!");
                break;
            } else {
                battlefield[usersLine][usersColumn] = "*";
            }
            paintBattlefield(battlefield);
        }
    }
}
