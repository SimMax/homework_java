package homework1;

import java.util.Arrays;

public class XArray {
    private final static int DEFAULT_SIZE = 4;
    private final double K = 1.5;

    private int[] storage;
    private int index;

    public XArray() {
        this(DEFAULT_SIZE);
    }

    public XArray(int size) {
        index = 0;
        storage = new int[size];
    }

    public void push(int value) {
        if (index >= storage.length) {
            resize();
        }
        storage[index++] = value;
    }

    private void resize() {
        int new_length = (int)( storage.length * K );
        int[] storage_new = new int[new_length];
        System.arraycopy(storage, 0, storage_new, 0, storage.length);
        storage = storage_new;
    }

    int[] get() {
        int[] result = new int[index];
        System.arraycopy(storage, 0, result, 0, index);
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("--------");
        sb.append(Arrays.toString(this.get()));
        sb.append("--------");
        return sb.toString();
    }
}
