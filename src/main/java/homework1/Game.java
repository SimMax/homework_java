package homework1;

import java.util.Scanner;

public class Game {
    public static int getRandomNumber() {
        return (int)(Math.random()*101);
    }

    public static String getUsersName() {
        Scanner sсan = new Scanner(System.in);
        System.out.println("Let the game begin!");
        System.out.print("Enter your name: ");
        String name = sсan.nextLine();
        return name;
    }

    public static int getUsersNumber() {
        Scanner sсan = new Scanner(System.in);
        int number;
        do {
            System.out.println("Enter a number from 0 to 100: ");
            while (!sсan.hasNextInt()) {
                System.out.println("Error! Enter a number from 0 to 100:");
                sсan.next();
            }
            number = sсan.nextInt();
            if (number < 0 || number > 100){
                System.out.println("Error! Enter a number from 0 to 100:");
                sсan.next();
            }
        } while (number < 0 || number > 100);
        return number;
    }

    public static void main(String[] args) {
        XArray arr_for_numbers = new XArray();
        String name = getUsersName();

        int randomNumber = getRandomNumber();
//        System.out.println(randomNumber);

        while (true){
            int number = getUsersNumber();
            arr_for_numbers.push(number);

            if(number < randomNumber){
                System.out.println("Your number is too small. Please, try again.");
            } else if(number > randomNumber){
                System.out.println("Your number is too big. Please, try again.");
            } else if (randomNumber == number){
                System.out.printf("Congratulations, %s. You guessed the number: %d.\n", name, number);
                System.out.println(arr_for_numbers);
                break;
            }
        }
    }
}
