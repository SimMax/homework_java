package homework7;

public class Main {
    public static void main(String[] args) {
        Woman mother = new Woman("Mary", "Mead", 1970);
        Man father = new Man("Tom", "Red", 1967);
        Dog dog = new Dog("Rock", 5, 75, new String[]{"eat", "drink", "sleep"});
        Man child_first = new Man("Michael", "Smith", 1990, 110,
                new String[][]{{DayOfWeek.Sun.getFullNameOfTheDay(),"go to fishing"},{DayOfWeek.Mon.getFullNameOfTheDay(),"go to gym"}});
        Woman child_second = new Woman("Ariel", "Jones", 1995, 90,
                new String[][]{{DayOfWeek.Sun.getFullNameOfTheDay(),"relax"}});
        Man child_third = new Man("Bob", "Williams", 1998, 110,
                new String[][]{{DayOfWeek.Thur.getFullNameOfTheDay(),"do the shopping"}});

        Family family = new Family(mother, father);
        mother.setFamily(family);
        father.setFamily(family);

        family.setPet(dog);

        family.addChild(child_first);
        child_first.setFamily(family);

        family.addChild(child_second);
        child_second.setFamily(family);

        family.addChild(child_third);
        child_third.setFamily(family);

        System.out.println("----------------------");
        System.out.println(family);
        System.out.println("countFamily: " + family.countFamily());
        System.out.printf("deleteChild: %s \n", family.deleteChild(child_third));
        System.out.println("countFamily: " + family.countFamily());

        System.out.println("----------------------");
        child_first.greetPet();
        child_first.describePet();
        child_first.feedPet(true);
        child_first.feedPet(false);
        child_first.feedPet(false);
        child_first.feedPet(false);

        System.out.println("----------------------");
        dog.eat();
        dog.respond();
        dog.foul();

    }
}
