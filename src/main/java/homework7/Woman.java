package homework7;

public final class Woman extends Human {
    public Woman(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Woman(String name, String surname, int year, int iq, String[][] schedule) {
        super(name, surname, year, iq, schedule);
    }

    public void makeup(){
        System.out.println("Makeup");
    }

    @Override
    public void greetPet(){
        System.out.println("greetPet");
    }
}
