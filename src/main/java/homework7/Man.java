package homework7;

public final class Man extends Human{
    public Man(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Man(String name, String surname, int year, int iq, String[][] schedule) {
        super(name, surname, year, iq, schedule);
    }

    public void repairCar(){
        System.out.println("RepairCar");
    }

    @Override
    public void greetPet(){
        System.out.println("greetPet");
    }
}
