package homework7;

import java.util.Arrays;

public class Fish extends Pet {
    private final Species species;

    public Fish(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
        this.species = Species.Fish;
    }

    @Override
    public void respond() {
        System.out.printf("Буль-буль, хозяин. Я %s %s. Я соскучилась! \n", species.getAnimalType(), nickname);
    }

    @Override
    public String toString() {
        return species.getAnimalType() + '{' +
                " nickname='" + nickname +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                ", habits=" + Arrays.toString(habits) +
                ", canFly=" + species.isCanFly() +
                ", numberOfLegs=" + species.getNumberOfLegs() +
                ", hasFur=" + species.isHasFur() +
                '}';
    }

    @Override
    public void finalize(){
        System.out.println("Deleted: " + this.toString());
    }
}
