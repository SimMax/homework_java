package homework7;

public enum Species {
    DomesticCat("DomesticCat", false, 4, true),
    RoboCat("RoboCat", false, 4, false),
    Dog("Dog", false, 4, true),
    Fish("Fish", false, 0, false),
    Hamster("Hamster", false, 4, true),
    Parrot("Parrot", true, 2, false),
    UNKNOWN(),
    ;

    private final String animalType;
    private final boolean canFly;
    private final int numberOfLegs;
    private final boolean hasFur;

    Species(){
        this("UNKNOWN", false, 0, false);
    }

    Species(String animalType, boolean canFly, int numberOfLegs, boolean hasFur) {
        this.animalType = animalType;
        this.canFly = canFly;
        this.numberOfLegs = numberOfLegs;
        this.hasFur = hasFur;
    }

    public String getAnimalType() {
        return animalType;
    }

    public boolean isCanFly() {
        return canFly;
    }

    public int getNumberOfLegs() {
        return numberOfLegs;
    }

    public boolean isHasFur() {
        return hasFur;
    }
}
