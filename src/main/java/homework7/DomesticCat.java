package homework7;

import java.util.Arrays;

public class DomesticCat extends Pet implements PetsFoul {
    private final Species species;

    public DomesticCat(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
        this.species = Species.DomesticCat;
    }

    @Override
    public void respond() {
        System.out.printf("Мяу-мяу. Я %s %s. Я соскучился! \n", species.getAnimalType(), nickname);
    }

    @Override
    public void foul() {
        System.out.println("Нужно хорошо замести следы...");
    }

    @Override
    public String toString() {
        return species.getAnimalType() + '{' +
                " nickname='" + nickname +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                ", habits=" + Arrays.toString(habits) +
                ", canFly=" + species.isCanFly() +
                ", numberOfLegs=" + species.getNumberOfLegs() +
                ", hasFur=" + species.isHasFur() +
                '}';
    }

    @Override
    public void finalize(){
        System.out.println("Deleted: " + this.toString());
    }
}
