package homework7;

public abstract class Pet {
     final String nickname;
     final int age;
     int trickLevel;
     String[] habits;

    public Pet(String nickname, int age, int trickLevel, String[] habits) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public void eat(){
        System.out.println("Я кушаю!");
    }

    public abstract void respond();
}
