package homework7;

import java.util.Arrays;

public class RoboCat extends Pet implements PetsFoul {
    private final Species species;

    public RoboCat(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
        this.species = Species.RoboCat;
    }

    @Override
    public void respond() {
        System.out.printf("Привет, хозяин. Я %s %s. Я соскучился! Мяу \n", species.getAnimalType(), nickname);
    }

    @Override
    public void foul() {
        System.out.println("Нужно хорошо замести следы...");
    }

    @Override
    public String toString() {
        return species.getAnimalType() + '{' +
                " nickname='" + nickname +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                ", habits=" + Arrays.toString(habits) +
                ", canFly=" + species.isCanFly() +
                ", numberOfLegs=" + species.getNumberOfLegs() +
                ", hasFur=" + species.isHasFur() +
                '}';
    }

    @Override
    public void finalize(){
        System.out.println("Deleted: " + this.toString());
    }
}
