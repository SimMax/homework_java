package homework7;

import java.util.Arrays;
import java.util.Objects;

public class Human {
    private final String name;
    private final String surname;
    private final int year;
    private Family family;
    private int iq;
    private String[][] schedule;

    static {
        System.out.println("Загружается класс Human");
    }

    {
        System.out.println("Human: новый экземпляр");
    }

    public Human(String name, String surname, int year) {
       this(name, surname, year,  90,  new String[][]{});
    }
    public Human(String name, String surname, int year, int iq, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public void greetPet(){
        if (family.getPet() != null){
            System.out.printf("Привет, %s! \n", family.getPet().nickname);
        } else {
            System.out.println("У тебя нет домашнего животного!");
        }
    }

    public boolean feedPet(boolean flag){
        if (family.getPet() != null){
            if(flag){
                System.out.printf("Я покормил %s. \n", family.getPet().nickname);
                return true;
            } else {
                if( family.getPet().trickLevel > (int)(Math.random()*101)){
                    System.out.printf("Хм... покормлю ка я %s. \n", family.getPet().nickname);
                    return true;
                } else {
                    System.out.printf("Думаю, %s не голоден. \n", family.getPet().nickname);
                    return false;
                }
            }
        } else {
            System.out.println("У тебя нет домашнего животного!");
            return false;
        }

    }

    public void describePet(){
        if (family.getPet() != null){
            System.out.printf("У меня есть %s, ему %s лет, он %s. \n", family.getPet().getSpecies().getAnimalType(), family.getPet().age,
                    family.getPet().trickLevel > 50 ? "очень хитрый" : "почти не хитрый");
        } else {
            System.out.println("У тебя нет домашнего животного!");
        }

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return year == human.year &&
                Objects.equals(name, human.name) &&
                Objects.equals(surname, human.surname) &&
                Objects.equals(family, human.family);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(name, surname, year, family, iq);
        result = 31 * result + Arrays.hashCode(schedule);
        return result;
    }

    @Override
    public String toString() {
        return "Human{" + "\n" +
                "\t name='" + name + '\'' + ",\n" +
                "\t surname='" + surname + '\'' + ",\n" +
                "\t year=" + year + ",\n" +
                "\t iq=" + iq + ",\n" +
                "\t schedule=" + Arrays.deepToString(schedule) + ",\n" +
                '}';
    }

    @Override
    public void finalize(){
        System.out.println("Deleted: " + this.toString());
    }
}
