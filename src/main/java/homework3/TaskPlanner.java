package homework3;

import java.util.Scanner;

public class TaskPlanner {
    public static String[][] changeScedule(String action, String[][] scedule) {
        String[] changeDay = action.split(" ");
        if(changeDay.length > 1) {
            Scanner scan = new Scanner(System.in);
            String day = changeDay[1];
            switch (day.toLowerCase()) {
                case "sunday": {
                    System.out.printf("Please, input new tasks for %s: \n" , day);
                    scedule[0][1] = scan.nextLine();
                    break;
                }
                case "monday": {
                    System.out.printf("Please, input new tasks for %s: \n" , day);
                    scedule[1][1] = scan.nextLine();
                    break;
                }
                case "tuesday": {
                    System.out.printf("Please, input new tasks for %s: \n" , day);
                    scedule[2][1] = scan.nextLine();
                    break;
                }
                case "wednesday": {
                    System.out.printf("Please, input new tasks for %s: \n" , day);
                    scedule[3][1] = scan.nextLine();
                    break;
                }
                case "thursday": {
                    System.out.printf("Please, input new tasks for %s: \n" , day);
                    scedule[4][1] = scan.nextLine();
                    break;
                }
                case "friday": {
                    System.out.printf("Please, input new tasks for %s: \n" , day);
                    scedule[5][1] = scan.nextLine();
                    break;
                }
                case "saturday": {
                    System.out.printf("Please, input new tasks for %s: \n" , day);
                    scedule[6][1] = scan.nextLine();
                    break;
                }
                default:{
                    System.out.println("Sorry, I don't understand you, please try again.");
                }
            }
        } else {
            System.out.println("Sorry, I don't understand what to change.");
        }
        return scedule;
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String action;
        String[][] scedule  = {
                {"Sunday", "do home work;"},
                {"Monday", "go to courses; watch a film;"},
                {"Tuesday", "do the shopping;"},
                {"Wednesday", "go to work;"},
                {"Thursday", "go fishing;"},
                {"Friday", "go to party;"},
                {"Saturday", "relax;"}
        };

        do {
            System.out.println("Please, input the day of the week:");
            action = scan.nextLine();
            switch (action.toLowerCase()) {
                case "sunday": {
                    System.out.printf("Your tasks for %s: %s \n", action, scedule[0][1]);
                    break;
                }
                case "monday": {
                    System.out.printf("Your tasks for %s: %s \n", action, scedule[1][1]);
                    break;
                }
                case "tuesday": {
                    System.out.printf("Your tasks for %s: %s \n", action, scedule[2][1]);
                    break;
                }
                case "wednesday": {
                    System.out.printf("Your tasks for %s: %s \n", action, scedule[3][1]);
                    break;
                }
                case "thursday": {
                    System.out.printf("Your tasks for %s: %s \n", action, scedule[4][1]);
                    break;
                }
                case "friday": {
                    System.out.printf("Your tasks for %s: %s \n", action, scedule[5][1]);
                    break;
                }
                case "saturday": {
                    System.out.printf("Your tasks for %s: %s \n", action, scedule[6][1]);
                    break;
                }
                case "exit":{
                    break;
                }
                default: {
                    if(action.toLowerCase().contains("change")){
                        scedule = changeScedule(action , scedule);
                    } else {
                        System.out.println("Sorry, I don't understand you, please try again.");
                    }
                }
            }
        } while ((!action.toLowerCase().equals("exit")));
    }
}
