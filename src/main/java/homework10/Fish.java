package homework10;

import java.util.HashSet;

public class Fish extends Pet {
    private final Species species;

    public Fish(String nickname, int age, int trickLevel, HashSet<String> habits) {
        super(nickname, age, trickLevel, habits);
        this.species = Species.FISH;
    }

    @Override
    public void respond() {
        System.out.printf("Буль-буль, хозяин. Я %s %s. Я соскучилась! \n", species.getAnimalType(), nickname);
    }

    @Override
    public String toString() {
        return species.name() + '{' +
                " nickname='" + nickname +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                ", habits=" + habits.toString() +
                ", canFly=" + species.isCanFly() +
                ", numberOfLegs=" + species.getNumberOfLegs() +
                ", hasFur=" + species.isHasFur() +
                '}';
    }

    @Override
    public void finalize(){
        System.out.println("Deleted: " + this.toString());
    }
}
