package homework10;

public enum DayOfWeek {
    MON("Monday"),
    TUE("Tuesday"),
    WED("Wednesday"),
    THUR("Thursday"),
    FRI("Friday"),
    SAT("Saturday"),
    SUN("Sunday")
    ;

    private final String fullTittle;

    DayOfWeek(String fullTittle){
        this.fullTittle = fullTittle;
    }

    public String getFullNameOfTheDay(){
        return this.fullTittle;
    }
}
