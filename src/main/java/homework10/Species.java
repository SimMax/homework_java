package homework10;

public enum Species {
    DOMESTICCAT("DomesticCat", false, 4, true),
    ROBOCAT("RoboCat", false, 4, false),
    DOG("Dog", false, 4, true),
    FISH("Fish", false, 0, false),
    HAMSTER("Hamster", false, 4, true),
    PARROT("Parrot", true, 2, false),
    UNKNOWN("Unknown"),
    ;

    private final String animalType;
    private boolean canFly;
    private int numberOfLegs;
    private boolean hasFur;

    Species(String animalType){
        this.animalType = animalType;
    }

    Species(String animalType, boolean canFly, int numberOfLegs, boolean hasFur) {
        this.animalType = animalType;
        this.canFly = canFly;
        this.numberOfLegs = numberOfLegs;
        this.hasFur = hasFur;
    }

    public String getAnimalType() {
        return animalType;
    }

    public boolean isCanFly() {
        return canFly;
    }

    public int getNumberOfLegs() {
        return numberOfLegs;
    }

    public boolean isHasFur() {
        return hasFur;
    }
}
