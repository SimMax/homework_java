package homework10;

import java.util.Date;
import java.util.HashMap;

public final class Woman extends Human {
    public Woman(String name, String surname, String birthDate) {
        this(name, surname, birthDate, 90);
    }

    public Woman(String name, String surname, String birthDate, int iq) {
        this(name, surname, birthDate,  iq, new HashMap<>());
    }

    public Woman(String name, String surname, String birthDate, int iq, HashMap<String, String> schedule) {
        super(name, surname, birthDate, iq, schedule);
    }

    public void makeup(){
        System.out.println("Makeup");
    }

    @Override
    public void greetPet(){
        if (family.getPets().size() > 0){
            for (Pet pet : family.getPets()) {
                System.out.printf("Привет, %s! \n", pet.nickname);
            }
        } else {
            System.out.println("У меня нет домашнего животного!");
        }
    }

    @Override
    public String toString() {
        Date date = new Date(birthDate);

        return "Woman{" + "\n" +
                "\t name='" + name + '\'' + ",\n" +
                "\t surname='" + surname + '\'' + ",\n" +
                "\t birthDate='" + (date.getDate() < 10 ? "0" + date.getDate() : date.getDate()) + "/"
                                + ((date.getMonth() + 1) < 10 ? "0" + (date.getMonth() + 1) : (date.getMonth() + 1)) + "/"
                                + date.getYear() + '\'' + ",\n" +
                "\t iq='" + iq + '\'' + ",\n" +
                "\t schedule='" + schedule.toString() + '\'' + ",\n" +
                '}';
    }
}
