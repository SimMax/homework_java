package homework10;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

public class Main {
    public static void main(String[] args) {
        Woman mother = new Woman("Mary", "Mead", "09/01/1970");
        Man father = new Man("Tom", "Red", "10/10/1965");
        Dog dog = new Dog("Rock", 5, 75, new HashSet<>(Arrays.asList("eat", "drink", "sleep")));
        Man child_first = new Man("Michael", "Smith", "09/01/1990", 110, new HashMap<String, String>() {{
            put(DayOfWeek.SUN.getFullNameOfTheDay(), "go to fishing");
            put(DayOfWeek.MON.getFullNameOfTheDay(), "go to gym");
        }});
        Woman child_second = new Woman("Ariel", "Jones", "24/12/1993", 90);
        Man child_third = new Man("Bob", "Williams", "09/08/1998", 110, new HashMap<String, String>() {{
            put(DayOfWeek.THUR.getFullNameOfTheDay(), "do the shopping");
        }});

        Family family = new Family(mother, father);
        family.setPet(dog);

        family.addChild(child_first);
        family.addChild(child_second);
        family.addChild(child_third);

        System.out.println(family);

        System.out.println(mother.describeAge());
        System.out.println(father.describeAge());
        System.out.println(child_first.describeAge());
        System.out.println(child_second.describeAge());
        System.out.println(child_third.describeAge());

    }
}
