package homework10;

import java.util.HashSet;

public class Dog extends Pet implements PetsFoul {
    private final Species species;

    public Dog(String nickname, int age, int trickLevel, HashSet<String> habits) {
        super(nickname, age, trickLevel, habits);
        this.species = Species.DOG;
    }

    @Override
    public void respond() {
        System.out.printf("Гав-гав. Я %s %s. Я соскучился! \n", species.getAnimalType(), nickname);
    }

    @Override
    public void foul() {
        System.out.println("Нужно хорошо замести следы...");
    }

    @Override
    public String toString() {
        return species.name() + '{' +
                " nickname='" + nickname + '\'' +
                ", age='" + age + '\'' +
                ", trickLevel='" + trickLevel + '\'' +
                ", habits='" + habits.toString() + '\'' +
                ", canFly='" + species.isCanFly() + '\'' +
                ", numberOfLegs='" + species.getNumberOfLegs() + '\'' +
                ", hasFur='" + species.isHasFur() + '\'' +
                '}';
    }

    @Override
    public void finalize(){
        System.out.println("Deleted: " + this.toString());
    }
}
