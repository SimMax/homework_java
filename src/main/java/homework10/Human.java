package homework10;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class Human {
    public final String name;
    public final String surname;
    public final long birthDate;
    public Family family;
    public int iq;
    public Map<String, String> schedule;

    public Human(String name, String surname, String birthDate, int iq, Map<String, String> schedule) {
        this.name = name;
        this.surname = surname;
        this.birthDate = getMilliseconds(birthDate);
        this.iq = iq;
        this.schedule = schedule;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public void greetPet(){
        if (family.getPets().size() > 0){
            for (Pet pet : family.getPets()) {
                System.out.printf("Привет, %s! \n", pet.nickname);
            }
        } else {
            System.out.println("У меня нет домашнего животного!");
        }
    }

    public boolean feedPet(boolean flag){
        if (family.getPets().size() > 0){
            if(flag){
                for (Pet pet : family.getPets()) {
                    System.out.printf("Я покормил %s. \n", pet.nickname);
                }
                flag = true;
            } else {
                for (Pet pet : family.getPets()) {
                    if( pet.trickLevel > (int)(Math.random()*101)){
                        System.out.printf("Хм... покормлю ка я %s. \n", pet.nickname);
                        flag = true;
                    } else {
                        System.out.printf("Думаю, %s не голоден. \n", pet.nickname);
                        flag = false;
                    }
                }
            }
        } else {
            System.out.println("У меня нет домашнего животного!");
            flag = false;
        }
        return flag;
    }

    public void describePet(){
        if (family.getPets().size() > 0){
            for (Pet pet : family.getPets()) {
                System.out.printf("У меня есть %s, ему %s лет, он %s. \n", pet.nickname, pet.age,
                        pet.trickLevel > 50 ? "очень хитрый" : "почти не хитрый");
            }
        } else {
            System.out.println("У меня нет домашнего животного!");
        }
    }

    private long getMilliseconds(String birthDate){
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
        Date date;
        try {
            date = format.parse(birthDate);
            return date.getTime();
        } catch (ParseException e) {
            System.out.println("Неверный формат даты: " + birthDate);
            return 0;
        }
    }

    public String describeAge() {
        if(birthDate != 0){
            long diffInMilliseconds = new Date().getTime() - new Date(birthDate).getTime();
            Calendar c = Calendar.getInstance();
            c.setTimeInMillis(diffInMilliseconds);

            int years = c.get(Calendar.YEAR) - 1970;
            int months = c.get(Calendar.MONTH);
            int days = c.get(Calendar.DAY_OF_MONTH) - 1;

            return "Со дня рождения " + name + " " + surname +   " прошло " + years + " лет, " + months + " месяцев и " + days + " дней.";
        }
        return "У " + name + " " + surname + " был неверный формат даты.";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return birthDate == human.birthDate &&
                iq == human.iq &&
                Objects.equals(name, human.name) &&
                Objects.equals(surname, human.surname) &&
                Objects.equals(family, human.family) &&
                Objects.equals(schedule, human.schedule);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, birthDate, family, iq, schedule);
    }

    @Override
    public void finalize(){
        System.out.println("Deleted: " + this.toString());
    }
}
