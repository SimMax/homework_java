package homework4_5_6;

public enum DayOfWeek {
    Mon("Monday"),
    Tue("Tuesday"),
    Wed("Wednesday"),
    Thur("Thursday"),
    Fri("Friday"),
    Sat("Saturday"),
    Sun("Sunday")
    ;

    private final String fullTittle;

    DayOfWeek(String fullTittle){
        this.fullTittle = fullTittle;
    }

    public String getFullNameOfTheDay(){
        return this.fullTittle;
    }
}
