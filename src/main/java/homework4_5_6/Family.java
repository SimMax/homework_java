package homework4_5_6;

import java.util.Arrays;
import java.util.Objects;

public class Family {
    private final Human mother;
    private final Human father;
    private Human[] children = new Human[]{};
    private Pet pet;

    static {
        System.out.println("Загружается класс Family");
    }

    {
        System.out.println("Family: новый экземпляр");
    }

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public Pet getPet() {
        return pet;
    }

    public void addChild(Human child) {
        Human[] newChild = new Human[children.length + 1];
        System.arraycopy(children, 0, newChild, 0, children.length);
        newChild[children.length] = child;
        this.children = newChild;
    }

    public boolean deleteChild(Human child) {
        Human[] newChild = new Human[children.length - 1];
        boolean flag = false;
        int index = 0;
        for(int i = 0; i < children.length; i++) {
            if (children[i].equals(child)) {
                flag = true;
                index = i;
                break;
            }
        }
        if (flag){
            System.arraycopy(children, 0, newChild, 0, index );
            System.arraycopy(children, index + 1, newChild, index, children.length - index - 1);
            children = newChild;
            return true;
        } else {
            return false;
        }
    }

    public int countFamily() {
        return children.length + (mother == null ? 0 : 1) + (father == null ? 0 : 1);
    }

    public Human[] _get_children_for_test() {
        return children;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(mother, family.mother) &&
                Objects.equals(father, family.father) &&
                Arrays.equals(children, family.children) &&
                Objects.equals(pet, family.pet);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(mother, father, pet);
        result = 31 * result + Arrays.hashCode(children);
        return result;
    }

    @Override
    public String toString() {
        return "Family{" +
                "mother=" + mother +
                ", father=" + father +
                ", children=" + Arrays.toString(children) +
                ", pet=" + pet +
                '}';
    }

    @Override
    public void finalize(){
        System.out.println("Deleted: " + this.toString());
    }
}
