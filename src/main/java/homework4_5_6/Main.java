package homework4_5_6;

public class Main {
    public static void main(String[] args) {
        Human mother = new Human("Mary", "Mead", 1970);
        Human father = new Human("Tom", "Red", 1967);
        Pet dog = new Pet(Species.Dog,"Rock", 5, 75, new String[]{"eat", "drink", "sleep"});
        Human child_first = new Human("Michael", "Smith", 1990, 110,
                new String[][]{{DayOfWeek.Sun.getFullNameOfTheDay(),"go to fishing"},{DayOfWeek.Mon.getFullNameOfTheDay(),"go to gym"}});
        Human child_second = new Human("Ariel", "Jones", 1995, 90,
                new String[][]{{DayOfWeek.Sun.getFullNameOfTheDay(),"relax"}});
        Human child_third = new Human("Bob", "Williams", 1998, 110,
                new String[][]{{DayOfWeek.Thur.getFullNameOfTheDay(),"do the shopping"}});

        Family family = new Family(mother, father);
        mother.setFamily(family);
        father.setFamily(family);

        family.setPet(dog);

        family.addChild(child_first);
        child_first.setFamily(family);

        family.addChild(child_second);
        child_second.setFamily(family);

        family.addChild(child_third);
        child_third.setFamily(family);

        System.out.println("----------------------");
        System.out.println(family);
        System.out.println("countFamily: " + family.countFamily());
        System.out.printf("deleteChild: %s \n", family.deleteChild(child_third));
        System.out.println("countFamily: " + family.countFamily());

        System.out.println("----------------------");
        child_first.greetPet();
        child_first.describePet();
        child_first.feedPet(true);
        child_first.feedPet(false);
        child_first.feedPet(false);
        child_first.feedPet(false);

        System.out.println("----------------------");
        dog.eat();
        dog.respond();
        dog.foul();

//        int count = 0;
//        while (count++ != 10000000){
//            Human human = new Human("Audrey", "Mead", 1970);
//        }
    }
}
