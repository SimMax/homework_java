package homework4_5_6;

import org.junit.jupiter.api.Test;
import java.util.Arrays;
import static org.junit.jupiter.api.Assertions.assertEquals;

class HumanTest {

    @Test
    void to_string() {
        String name = "Michael";
        String surname = "Smith";
        int year = 1990;
        int iq = 110;
        String[][] schedule = new String[][]{{DayOfWeek.Sun.getFullNameOfTheDay(), "go to fishing"}, {DayOfWeek.Mon.getFullNameOfTheDay(), "go to gym"}};

        Human human = new Human(name, surname, year, iq, schedule);

        String human_string = "Human{" + "\n" +
                "\t name='" + name + '\'' + ",\n" +
                "\t surname='" + surname + '\'' + ",\n" +
                "\t year=" + year + ",\n" +
                "\t iq=" + iq + ",\n" +
                "\t schedule=" + Arrays.deepToString(schedule) + ",\n" +
                '}';

        assertEquals(human_string, human.toString());
    }

    @Test
    void equals_this_eq_another() {
        Human human_first = new Human("Michael", "Smith", 1990, 110,
                new String[][]{{DayOfWeek.Sun.getFullNameOfTheDay(),"go to fishing"},{DayOfWeek.Mon.getFullNameOfTheDay(),"go to gym"}});
        assertEquals(true, human_first.equals(human_first));
    }

    @Test
    void equals_another_eq_null() {
        Human human_first = new Human("Michael", "Smith", 1990, 110,
                new String[][]{{DayOfWeek.Sun.getFullNameOfTheDay(),"go to fishing"},{DayOfWeek.Mon.getFullNameOfTheDay(),"go to gym"}});
        assertEquals(false, human_first.equals(null));
    }

    @Test
    void equals_another_diff_type() {
        Human human = new Human("Michael", "Smith", 1990, 110,
                new String[][]{{DayOfWeek.Sun.getFullNameOfTheDay(),"go to fishing"},{DayOfWeek.Mon.getFullNameOfTheDay(),"go to gym"}});
        Pet pet = new Pet(Species.Dog,"Rock", 5, 75, new String[]{"eat", "drink", "sleep"});
        assertEquals(false, human.equals(pet));
    }
}