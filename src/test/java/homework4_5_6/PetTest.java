package homework4_5_6;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;


class PetTest {

    @Test
    void to_string() {
        Species species = Species.Dog;
        String nickname = "Rock";
        int age = 5;
        int trickLevel = 75;
        String[] habits = new String[]{"eat", "drink", "sleep"};

        Pet pet = new Pet(species, nickname, age, trickLevel, habits);

        String pet_string = "Pet{" +
                " animalType=" + species.getAnimalType() +
                ", canFly=" + species.isCanFly() +
                ", numberOfLegs=" + species.getNumberOfLegs() +
                ", hasFur=" + species.isHasFur() +
                ", nickname='" + nickname +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                ", habits=" + Arrays.toString(habits) +
                '}';

        assertEquals(pet_string, pet.toString());
    }

    @Test
    void equals_this_eq_another() {
        Pet pet_first = new Pet(Species.Dog,"Rock", 5, 75, new String[]{"eat", "drink", "sleep"});
        assertEquals(true, pet_first.equals(pet_first));
    }

    @Test
    void equals_another_eq_null() {
        Pet pet_first = new Pet(Species.Dog,"Rock", 5, 75, new String[]{"eat", "drink", "sleep"});
        assertEquals(false, pet_first.equals(null));
    }

    @Test
    void equals_another_diff_type() {
        Pet pet = new Pet(Species.Dog,"Rock", 5, 75, new String[]{"eat", "drink", "sleep"});
        Human human = new Human("Michael", "Smith", 1990, 110,
                new String[][]{{DayOfWeek.Sun.getFullNameOfTheDay(),"go to fishing"},{DayOfWeek.Mon.getFullNameOfTheDay(),"go to gym"}});
        assertEquals(false, pet.equals(human));
    }
}