package homework4_5_6;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FamilyTest {

    @Test
    void to_string() {
        Human mother = new Human("Mary", "Mead", 1970);
        Human father = new Human("Tom", "Red", 1967);
        Pet dog = new Pet(Species.Dog,"Rock", 5, 75, new String[]{"eat", "drink", "sleep"});
        Human child = new Human("Michael", "Smith", 1990, 110,
                new String[][]{{DayOfWeek.Sun.getFullNameOfTheDay(),"go to fishing"},{DayOfWeek.Mon.getFullNameOfTheDay(),"go to gym"}});

        Family family = new Family(mother, father);
        family.setPet(dog);
        family.addChild(child);

        String family_string = "Family{" +
                "mother=" + mother +
                ", father=" + father +
                ", children=" + Arrays.toString(new Human[]{child}) +
                ", pet=" + dog +
                '}';

        assertEquals(family_string, family.toString());
    }

    @Test
    void addChild() {
        Human mother = new Human("Mary", "Mead", 1970);
        Human father = new Human("Tom", "Red", 1967);

        Human child_first = new Human("Michael", "Smith", 1990, 110,
                new String[][]{{DayOfWeek.Sun.getFullNameOfTheDay(),"go to fishing"},{DayOfWeek.Mon.getFullNameOfTheDay(),"go to gym"}});
        Human child_second = new Human("Ariel", "Jones", 1995, 90,
                new String[][]{{DayOfWeek.Sun.getFullNameOfTheDay(),"relax"}});
        Human child_third = new Human("Bob", "Williams", 1998, 110,
                new String[][]{{DayOfWeek.Thur.getFullNameOfTheDay(),"do the shopping"}});

        Family family = new Family(mother, father);
        mother.setFamily(family);
        father.setFamily(family);
        assertEquals(0, family._get_children_for_test().length);

        family.addChild(child_first);
        child_first.setFamily(family);
        assertEquals(1, family._get_children_for_test().length);

        family.addChild(child_second);
        child_second.setFamily(family);
        assertEquals(2, family._get_children_for_test().length);

        family.addChild(child_third);
        child_third.setFamily(family);
        assertEquals(3, family._get_children_for_test().length);
    }

    @Test
    void countFamily() {
        Human mother = new Human("Mary", "Mead", 1970);
        Human father = new Human("Tom", "Red", 1967);

        Human child_first = new Human("Michael", "Smith", 1990, 110,
                new String[][]{{DayOfWeek.Sun.getFullNameOfTheDay(),"go to fishing"},{DayOfWeek.Mon.getFullNameOfTheDay(),"go to gym"}});
        Human child_second = new Human("Ariel", "Jones", 1995, 90,
                new String[][]{{DayOfWeek.Sun.getFullNameOfTheDay(),"relax"}});
        Human child_third = new Human("Bob", "Williams", 1998, 110,
                new String[][]{{DayOfWeek.Thur.getFullNameOfTheDay(),"do the shopping"}});

        Family family = new Family(mother, father);
        mother.setFamily(family);
        father.setFamily(family);
        assertEquals(2, family.countFamily());

        family.addChild(child_first);
        child_first.setFamily(family);
        assertEquals(3, family.countFamily());

        family.addChild(child_second);
        child_second.setFamily(family);
        assertEquals(4, family.countFamily());

        family.addChild(child_third);
        child_third.setFamily(family);
        assertEquals(5, family.countFamily());
    }

    @Test
    void deleteChild_correct_type() {
        Human mother = new Human("Mary", "Mead", 1970);
        Human father = new Human("Tom", "Red", 1967);

        Human child_first = new Human("Michael", "Smith", 1990, 110,
                new String[][]{{DayOfWeek.Sun.getFullNameOfTheDay(),"go to fishing"},{DayOfWeek.Mon.getFullNameOfTheDay(),"go to gym"}});
        Human child_second = new Human("Ariel", "Jones", 1995, 90,
                new String[][]{{DayOfWeek.Sun.getFullNameOfTheDay(),"relax"}});
        Human child_third = new Human("Bob", "Williams", 1998, 110,
                new String[][]{{DayOfWeek.Thur.getFullNameOfTheDay(),"do the shopping"}});

        Family family = new Family(mother, father);
        mother.setFamily(family);
        father.setFamily(family);

        family.addChild(child_first);
        child_first.setFamily(family);

        family.addChild(child_second);
        child_second.setFamily(family);

        family.addChild(child_third);
        child_third.setFamily(family);

        assertEquals(3, family._get_children_for_test().length);
        assertEquals(true, family.deleteChild(child_third));
        assertEquals(2, family._get_children_for_test().length);

        for (Human child: family._get_children_for_test()) {
            assertEquals(false, child_third.equals(child));
        }
    }

    @Test
    void deleteChild_wrong_type() {
        Human mother = new Human("Mary", "Mead", 1970);
        Human father = new Human("Tom", "Red", 1967);

        Human child_first = new Human("Michael", "Smith", 1990, 110,
                new String[][]{{DayOfWeek.Sun.getFullNameOfTheDay(),"go to fishing"},{DayOfWeek.Mon.getFullNameOfTheDay(),"go to gym"}});
        Human child_second = new Human("Ariel", "Jones", 1995, 90,
                new String[][]{{DayOfWeek.Sun.getFullNameOfTheDay(),"relax"}});
        Human stranger = new Human("Bob", "Williams", 1998, 110,
                new String[][]{{DayOfWeek.Thur.getFullNameOfTheDay(),"do the shopping"}});

        Family family = new Family(mother, father);
        mother.setFamily(family);
        father.setFamily(family);

        family.addChild(child_first);
        child_first.setFamily(family);

        family.addChild(child_second);
        child_second.setFamily(family);

        assertEquals(2, family._get_children_for_test().length);
        assertEquals(false, family.deleteChild(stranger));
        assertEquals(2, family._get_children_for_test().length);
    }

    @Test
    void equals_this_eq_another() {
        Human mother = new Human("Mary", "Mead", 1970);
        Human father = new Human("Tom", "Red", 1967);

        Human child_first = new Human("Michael", "Smith", 1990, 110,
                new String[][]{{DayOfWeek.Sun.getFullNameOfTheDay(),"go to fishing"},{DayOfWeek.Mon.getFullNameOfTheDay(),"go to gym"}});
        Human child_second = new Human("Ariel", "Jones", 1995, 90,
                new String[][]{{DayOfWeek.Sun.getFullNameOfTheDay(),"relax"}});
        Family family_first = new Family(mother, father);
        mother.setFamily(family_first);
        father.setFamily(family_first);

        family_first.addChild(child_first);
        child_first.setFamily(family_first);

        family_first.addChild(child_second);
        child_second.setFamily(family_first);

        assertEquals(true, family_first.equals(family_first));
    }

    @Test
    void equals_another_eq_null() {
        Human mother = new Human("Mary", "Mead", 1970);
        Human father = new Human("Tom", "Red", 1967);

        Human child_first = new Human("Michael", "Smith", 1990, 110,
                new String[][]{{DayOfWeek.Sun.getFullNameOfTheDay(),"go to fishing"},{DayOfWeek.Mon.getFullNameOfTheDay(),"go to gym"}});
        Human child_second = new Human("Ariel", "Jones", 1995, 90,
                new String[][]{{DayOfWeek.Sun.getFullNameOfTheDay(),"relax"}});
        Family family_first = new Family(mother, father);
        mother.setFamily(family_first);
        father.setFamily(family_first);

        family_first.addChild(child_first);
        child_first.setFamily(family_first);

        family_first.addChild(child_second);
        child_second.setFamily(family_first);

        assertEquals(false, family_first.equals(null));
    }

    @Test
    void equals_another_diff_type() {
        Human mother_first = new Human("Mary", "Mead", 1970);
        Human father_first = new Human("Tom", "Red", 1967);

        Human child_first = new Human("Michael", "Smith", 1990, 110,
                new String[][]{{DayOfWeek.Sun.getFullNameOfTheDay(),"go to fishing"},{DayOfWeek.Mon.getFullNameOfTheDay(),"go to gym"}});
        Human child_second = new Human("Ariel", "Jones", 1995, 90,
                new String[][]{{DayOfWeek.Sun.getFullNameOfTheDay(),"relax"}});
        Family family_first = new Family(mother_first, father_first);
        mother_first.setFamily(family_first);
        father_first.setFamily(family_first);

        family_first.addChild(child_first);
        child_first.setFamily(family_first);

        family_first.addChild(child_second);
        child_second.setFamily(family_first);

        Human mother_second = new Human("Ariel", "Jones", 1995, 90,
                new String[][]{{DayOfWeek.Sun.getFullNameOfTheDay(),"relax"}});
        Human father_second = new Human("Michael", "Smith", 1990, 110,
                new String[][]{{DayOfWeek.Sun.getFullNameOfTheDay(),"go to fishing"},{DayOfWeek.Mon.getFullNameOfTheDay(),"go to gym"}});
        Family family_second = new Family(mother_second, father_second);
        mother_second.setFamily(family_second);
        father_second.setFamily(family_second);

        assertEquals(false, family_first.equals(family_second));
    }
}