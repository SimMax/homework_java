package homework8;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;

class DogTest {

    @Test
    void to_string() {
        Species species = Species.DOG;
        String nickname = "Rock";
        int age = 5;
        int trickLevel = 75;
        HashSet<String> habits = new HashSet<>(Arrays.asList("eat", "drink", "sleep"));

        Dog dog = new Dog("Rock", 5, 75, new HashSet<>(Arrays.asList("eat", "drink", "sleep")));

        String dog_string = "DOG{" +
                " nickname='" + nickname +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                ", habits=" + habits.toString() +
                ", canFly=" + species.isCanFly() +
                ", numberOfLegs=" + species.getNumberOfLegs() +
                ", hasFur=" + species.isHasFur() +
                '}';

        assertEquals(dog_string, dog.toString());
    }

}