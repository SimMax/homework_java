package homework8;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;

class FamilyTest {
    @Test
    void to_string() {
        Woman mother = new Woman("Mary", "Mead", 1970);
        Man father = new Man("Tom", "Red", 1967);
        Dog dog = new Dog("Rock", 5, 75, new HashSet<>(Arrays.asList("eat", "drink", "sleep")));
        Man child = new Man("Michael", "Smith", 1990, 110, new HashMap<String, String>() {{
            put(DayOfWeek.SUN.getFullNameOfTheDay(), "go to fishing");
            put(DayOfWeek.MON.getFullNameOfTheDay(), "go to gym");
        }});

        Family family = new Family(mother, father);
        family.setPet(dog);
        family.addChild(child);

        String family_string = "Family{" +
                "mother=" + mother +
                ", father=" + father +
                ", children=" + Arrays.toString(new Human[]{child}) +
                ", pets=" + Arrays.toString(new Dog[]{dog}) +
                '}';

        assertEquals(family_string, family.toString());
    }

    @Test
    void addChild() {
        Woman mother = new Woman("Mary", "Mead", 1970);
        Man father = new Man("Tom", "Red", 1967);
        Man child_first = new Man("Michael", "Smith", 1990, 110, new HashMap<String, String>() {{
            put(DayOfWeek.SUN.getFullNameOfTheDay(), "go to fishing");
            put(DayOfWeek.MON.getFullNameOfTheDay(), "go to gym");
        }});
        Woman child_second = new Woman("Ariel", "Jones", 1995, 90, new HashMap<String, String>() {{
            put(DayOfWeek.SUN.getFullNameOfTheDay(), "relax");
        }});
        Man child_third = new Man("Bob", "Williams", 1998, 110, new HashMap<String, String>() {{
            put(DayOfWeek.THUR.getFullNameOfTheDay(), "do the shopping");
        }});

        Family family = new Family(mother, father);
        assertEquals(0, family._get_children_for_test().size());

        family.addChild(child_first);
        assertEquals(1, family._get_children_for_test().size());

        family.addChild(child_second);
        assertEquals(2, family._get_children_for_test().size());

        family.addChild(child_third);
        assertEquals(3, family._get_children_for_test().size());
    }

    @Test
    void countFamily() {
        Woman mother = new Woman("Mary", "Mead", 1970);
        Man father = new Man("Tom", "Red", 1967);
        Man child_first = new Man("Michael", "Smith", 1990, 110, new HashMap<String, String>() {{
            put(DayOfWeek.SUN.getFullNameOfTheDay(), "go to fishing");
            put(DayOfWeek.MON.getFullNameOfTheDay(), "go to gym");
        }});
        Woman child_second = new Woman("Ariel", "Jones", 1995, 90, new HashMap<String, String>() {{
            put(DayOfWeek.SUN.getFullNameOfTheDay(), "relax");
        }});
        Man child_third = new Man("Bob", "Williams", 1998, 110, new HashMap<String, String>() {{
            put(DayOfWeek.THUR.getFullNameOfTheDay(), "do the shopping");
        }});

        Family family = new Family(mother, father);
        assertEquals(2, family.countFamily());

        family.addChild(child_first);
        assertEquals(3, family.countFamily());

        family.addChild(child_second);
        assertEquals(4, family.countFamily());

        family.addChild(child_third);
        assertEquals(5, family.countFamily());
    }

    @Test
    void deleteChild_correct_type() {
        Woman mother = new Woman("Mary", "Mead", 1970);
        Man father = new Man("Tom", "Red", 1967);
        Man child_first = new Man("Michael", "Smith", 1990, 110, new HashMap<String, String>() {{
            put(DayOfWeek.SUN.getFullNameOfTheDay(), "go to fishing");
            put(DayOfWeek.MON.getFullNameOfTheDay(), "go to gym");
        }});
        Woman child_second = new Woman("Ariel", "Jones", 1995, 90, new HashMap<String, String>() {{
            put(DayOfWeek.SUN.getFullNameOfTheDay(), "relax");
        }});
        Man child_third = new Man("Bob", "Williams", 1998, 110, new HashMap<String, String>() {{
            put(DayOfWeek.THUR.getFullNameOfTheDay(), "do the shopping");
        }});

        Family family = new Family(mother, father);

        family.addChild(child_first);
        family.addChild(child_second);
        family.addChild(child_third);

        assertEquals(3, family._get_children_for_test().size());
        assertEquals(true, family.deleteChild(child_third));
        assertEquals(2, family._get_children_for_test().size());

        for (Human child: family._get_children_for_test()) {
            assertEquals(false, child_third.equals(child));
        }
    }

    @Test
    void deleteChild_wrong_type() {
        Woman mother = new Woman("Mary", "Mead", 1970);
        Man father = new Man("Tom", "Red", 1967);
        Man child_first = new Man("Michael", "Smith", 1990, 110, new HashMap<String, String>() {{
            put(DayOfWeek.SUN.getFullNameOfTheDay(), "go to fishing");
            put(DayOfWeek.MON.getFullNameOfTheDay(), "go to gym");
        }});
        Woman child_second = new Woman("Ariel", "Jones", 1995, 90, new HashMap<String, String>() {{
            put(DayOfWeek.SUN.getFullNameOfTheDay(), "relax");
        }});
        Man stranger = new Man("Bob", "Williams", 1998, 110, new HashMap<String, String>() {{
            put(DayOfWeek.THUR.getFullNameOfTheDay(), "do the shopping");
        }});

        Family family = new Family(mother, father);

        family.addChild(child_first);
        family.addChild(child_second);

        assertEquals(2, family._get_children_for_test().size());
        assertEquals(false, family.deleteChild(stranger));
        assertEquals(2, family._get_children_for_test().size());
    }

    @Test
    void equals_this_eq_another() {
        Woman mother = new Woman("Mary", "Mead", 1970);
        Man father = new Man("Tom", "Red", 1967);
        Man child = new Man("Michael", "Smith", 1990, 110, new HashMap<String, String>() {{
            put(DayOfWeek.SUN.getFullNameOfTheDay(), "go to fishing");
            put(DayOfWeek.MON.getFullNameOfTheDay(), "go to gym");
        }});

        Family family = new Family(mother, father);
        family.addChild(child);

        assertEquals(true, family.equals(family));
    }

    @Test
    void equals_another_eq_null() {
        Woman mother = new Woman("Mary", "Mead", 1970);
        Man father = new Man("Tom", "Red", 1967);
        Man child = new Man("Michael", "Smith", 1990, 110, new HashMap<String, String>() {{
            put(DayOfWeek.SUN.getFullNameOfTheDay(), "go to fishing");
            put(DayOfWeek.MON.getFullNameOfTheDay(), "go to gym");
        }});

        Family family = new Family(mother, father);
        family.addChild(child);

        assertEquals(false, family.equals(null));
    }

    @Test
    void equals_another_diff_type() {
        Woman mother_first = new Woman("Mary", "Mead", 1970);
        Man father_first = new Man("Tom", "Red", 1967);
        Man child = new Man("Michael", "Smith", 1990, 110, new HashMap<String, String>() {{
            put(DayOfWeek.SUN.getFullNameOfTheDay(), "go to fishing");
            put(DayOfWeek.MON.getFullNameOfTheDay(), "go to gym");
        }});
        Family family_first = new Family(mother_first, father_first);
        family_first.addChild(child);

        Woman mother_second = new Woman("Ariel", "Jones", 1995, 90, new HashMap<String, String>() {{
            put(DayOfWeek.SUN.getFullNameOfTheDay(), "relax");
        }});
        Man father_second = new Man("Bob", "Williams", 1998, 110, new HashMap<String, String>() {{
            put(DayOfWeek.THUR.getFullNameOfTheDay(), "do the shopping");
        }});
        Family family_second = new Family(mother_second, father_second);

        assertEquals(false, family_first.equals(family_second));
    }

}